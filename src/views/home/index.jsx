import React, { useEffect, useState } from 'react';
import { translateAPI } from '../../utils';
import Styles from './index.module.scss';

export default function Home(props) {
  const { languages } = props;
  const [startingPhrase, setStartingPhrase] = useState('');
  // const [currentPhrase, setCurrentPhrase] = useState('');
  const [languageCount, setLanguageCount] = useState(5);
  const [languageList, setLanguageList] = useState([]);

  let translateList;
  let currentPhrase;
  let currentIndex;

  const randomize = () => {
    const list = [];
    for (let i = 0; i < languageCount; i += 1) {
      if (i === 0) {
        list.push(languages[Math.floor(Math.random() * languages.length)]);
      } else {
        let random = languages[Math.floor(Math.random() * languages.length)];
        while (random === languages[languageCount - 1]) {
          random = languages[Math.floor(Math.random() * languages.length)];
        }
        list.push(random);
      }
    }
    list.push({ code: 'en', name: 'English' });
    setLanguageList(list);
  };

  useEffect(() => {
    if (languages.length) randomize();
  }, [languages]);

  const translateOne = (language) => new Promise((resolve) => {
    const url = `${translateAPI}/translate`;
    const request = {
      q: currentPhrase,
      source: currentIndex >= 1 ? languageList[currentIndex - 1].code : 'auto',
      target: language.code,
    };
    console.log(request);
    fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(request),
    }).then((res) => res.json()).then((res) => {
      console.log(res);
      currentPhrase = res.translatedText;
      const newLanguageList = JSON.parse(JSON.stringify(languageList));
      newLanguageList[currentIndex].result = res.translatedText;
      setLanguageList(newLanguageList);
      currentIndex += 1;
      // setCurrentPhrase(res.translatedText);
      resolve();
    });
  });

  const translateAll = (callback) => {
    if (!translateList.length) {
      callback();
      return;
    }
    translateOne(translateList.shift()).then(() => {
      translateAll(callback);
    });
  };

  const completed = () => {
    console.log('all done');
  };

  const getLost = () => {
    // setCurrentPhrase(startingPhrase);
    currentPhrase = startingPhrase;
    currentIndex = 0;
    translateList = languageList.slice();
    translateAll(completed);
  };

  return (
    <div className={Styles.home}>
      <div className={Styles.controls}>
        <span>tranlste through:</span>
        <input type="range" min="2" max="20" defaultValue={languageCount} onChange={(e) => setLanguageCount(e.target.value)} />
        <span>{languageCount}</span>
        <span>languages</span>
        <button type="button" onClick={randomize}>Randomize</button>
      </div>
      <div className={Styles.prompt}>
        <input type="text" placeholder="Type Something" onChange={(e) => { setStartingPhrase(e.target.value); }} />
        <button type="button" onClick={getLost}>Get Lost!</button>
      </div>
      <ul className={Styles.languages}>
        {languageList.map((language, index) => (
          <li key={`${index}-${language.code}`}>
            <span>{language.name}</span>
            <span>-&gt;</span>
            <span>{language.result || '...'}</span>
          </li>
        ))}
      </ul>
      <p>{currentPhrase}</p>
    </div>
  );
}
